package cl.ufro.dci.auth.domain.model;

import java.util.List;

public class Company {
    private Long id;
    private String name;
    private String secretKey;
    private List<User> users;
    
    public Company(Long id, String name, String secretKey, List<User> users) {
        this.id = id;
        this.name = name;
        this.secretKey = secretKey;
        this.users = users;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSecretKey() {
        return secretKey;
    }
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    public List<User> getUsers() {
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean haveUser(Long userId) {
        return users.stream().anyMatch(user -> user.getId().equals(userId));
    }
}
