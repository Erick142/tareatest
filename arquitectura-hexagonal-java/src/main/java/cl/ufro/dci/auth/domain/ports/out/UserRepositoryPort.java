package cl.ufro.dci.auth.domain.ports.out;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;

public interface UserRepositoryPort {
    User save(User user);
    Optional<User> findById(Long id);
    List<User> findAll();
    Optional<User> update(User user);
    boolean deleteById(Long id);
}


