package cl.ufro.dci.auth.application.usecases;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.in.AddUserToCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;

public class AddUserToCompanyUseCaseImpl implements AddUserToCompanyUseCase {
    private final CompanyRepositoryPort companyRepositoryPort;
    private final UserRepositoryPort userRepositoryPort;
    

    public AddUserToCompanyUseCaseImpl(CompanyRepositoryPort companyRepositoryPort,
            UserRepositoryPort userRepositoryPort) {
        this.companyRepositoryPort = companyRepositoryPort;
        this.userRepositoryPort = userRepositoryPort;
    }


    @Override
    public Optional<Company> addUserToCompany(Long companyId, Long userId) {
        Optional<User> userOptional = userRepositoryPort.findById(userId);
        Optional<Company> companyOptional = companyRepositoryPort.findById(companyId);
        
        if(userOptional.isEmpty() || companyOptional.isEmpty()){
            return Optional.empty();
        }
        User user = userOptional.get();
        Company company = companyOptional.get();

        List<User> companyUsers = company.getUsers();

        if (companyUsers.stream().anyMatch(u -> u.getId().equals(userId))) {
            return Optional.empty();
        }

        companyUsers.add(user);

        company.setUsers(companyUsers);

        return companyRepositoryPort.update(company);
    }
    
}
