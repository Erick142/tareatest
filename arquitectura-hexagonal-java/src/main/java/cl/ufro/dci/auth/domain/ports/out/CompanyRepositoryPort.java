package cl.ufro.dci.auth.domain.ports.out;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;

public interface CompanyRepositoryPort {
    Company save(Company company);
    Optional<Company> findById(Long id);
    List<Company> findAll();
    Optional<Company> update(Company company);
    boolean deleteById(Long id);
    List<Company> findCompaniesByUserId(Long id);
}
