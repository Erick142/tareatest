package cl.ufro.dci.auth.domain.ports.in;

import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;

public interface UpdateCompanyUseCase {
    Optional<Company> updateCompany(Long id, Company company);
}
