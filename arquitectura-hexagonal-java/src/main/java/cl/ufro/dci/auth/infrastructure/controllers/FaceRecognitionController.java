package cl.ufro.dci.auth.infrastructure.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.ufro.dci.auth.infrastructure.adapters.FaceRecognicionAdapter;

@RestController
@RequestMapping("/api/face")
public class FaceRecognitionController {
    private final FaceRecognicionAdapter faceRecognicionAdapter;

    public FaceRecognitionController(FaceRecognicionAdapter faceRecognicionAdapter) {
        this.faceRecognicionAdapter = faceRecognicionAdapter;
    }

    /**
     * Obtiene las claves de la API externa de reconocimiento facial.
     * 
     * @return ResponseEntity con las claves y el código de estado HTTP correspondiente.
     */
    @GetMapping
    public ResponseEntity<Object> seeFaceApiKeys(){
        Map<String, String> keys = faceRecognicionAdapter.getApiKeys();
        return new ResponseEntity<>(keys, HttpStatus.OK);
    }

    /**
     * Cambia la clave de reconocimiento facial.
     * 
     * @param key La nueva clave de reconocimiento facial.
     * @return    ResponseEntity con las claves actualizadas y el código de estado HTTP correspondiente.
     */
    @PatchMapping("/recognition")
    public ResponseEntity<Object> changeRecognitionKey(@RequestParam String key){
        faceRecognicionAdapter.setFaceRecognitionKey(key);
        Map<String, String> keys =  faceRecognicionAdapter.getApiKeys();
        return new ResponseEntity<>(keys, HttpStatus.OK);
    }

    /**
     * Cambia la clave de detección facial.
     * 
     * @param key La nueva clave de detección facial.
     * @return    ResponseEntity con las claves actualizadas y el código de estado HTTP correspondiente.
     */
    @PatchMapping("/detection")
    public ResponseEntity<Object> changeDetectionKey(@RequestParam String key){
        faceRecognicionAdapter.setFaceDetectionKey(key);
        Map<String, String> keys =  faceRecognicionAdapter.getApiKeys();

        return new ResponseEntity<>(keys, HttpStatus.OK);
    }

    /**
     * Cambia la clave de verificación facial.
     * 
     * @param key La nueva clave de verificación facial.
     * @return    ResponseEntity con las claves actualizadas y el código de estado HTTP correspondiente.
     */
    @PatchMapping("/verification")
    public ResponseEntity<Object> changeVerificationKey(@RequestParam String key){
        faceRecognicionAdapter.setFaceVerificationKey(key);
        Map<String, String> keys =  faceRecognicionAdapter.getApiKeys();

        return new ResponseEntity<>(keys, HttpStatus.OK);
    }

    
}
