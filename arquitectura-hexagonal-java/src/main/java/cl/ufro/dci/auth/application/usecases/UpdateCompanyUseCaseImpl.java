package cl.ufro.dci.auth.application.usecases;

import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.ports.in.UpdateCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;

public class UpdateCompanyUseCaseImpl implements UpdateCompanyUseCase{

    private final CompanyRepositoryPort companyRepositoryPort;

    public UpdateCompanyUseCaseImpl(CompanyRepositoryPort companyRepositoryPort){
        this.companyRepositoryPort = companyRepositoryPort;
    }

    @Override
    public Optional<Company> updateCompany(Long id, Company company) {
        return companyRepositoryPort.update(company);
    }
    
}
