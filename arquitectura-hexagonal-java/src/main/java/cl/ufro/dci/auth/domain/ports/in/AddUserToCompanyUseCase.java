package cl.ufro.dci.auth.domain.ports.in;

import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;

public interface AddUserToCompanyUseCase {
    Optional<Company> addUserToCompany(Long companyId, Long userId); 
}
