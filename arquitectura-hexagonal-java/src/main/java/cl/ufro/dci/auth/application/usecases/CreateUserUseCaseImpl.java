package cl.ufro.dci.auth.application.usecases;

import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.in.CreateUserUseCase;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;

public class CreateUserUseCaseImpl implements CreateUserUseCase{
    private final UserRepositoryPort userRepositoryPort;
    

    public CreateUserUseCaseImpl(UserRepositoryPort userRepositoryPort) {
        this.userRepositoryPort = userRepositoryPort;
    }


    @Override
    public User createUser(User user) {
        return userRepositoryPort.save(user);
    }
    
}
