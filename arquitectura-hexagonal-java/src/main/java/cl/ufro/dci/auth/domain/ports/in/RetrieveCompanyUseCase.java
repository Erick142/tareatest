package cl.ufro.dci.auth.domain.ports.in;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;

public interface RetrieveCompanyUseCase {
    Optional<Company> getCompanyById(Long id);
    List<Company> getAllCompanies();
    List<Company> findCompaniesByUserId(Long userId);
}
