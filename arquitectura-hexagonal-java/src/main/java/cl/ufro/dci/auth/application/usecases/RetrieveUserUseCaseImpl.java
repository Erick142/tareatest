package cl.ufro.dci.auth.application.usecases;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.in.RetrieveUserUseCase;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;

public class RetrieveUserUseCaseImpl implements RetrieveUserUseCase {
    private final UserRepositoryPort userRepositoryPort;

    public RetrieveUserUseCaseImpl(UserRepositoryPort userRepositoryPort) {
        this.userRepositoryPort = userRepositoryPort;
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepositoryPort.findById(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepositoryPort.findAll();
    }
}
