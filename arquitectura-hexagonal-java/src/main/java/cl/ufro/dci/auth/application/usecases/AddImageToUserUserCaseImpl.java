package cl.ufro.dci.auth.application.usecases;

import java.io.File;

import cl.ufro.dci.auth.domain.ports.in.AddImageToUserUseCase;
import cl.ufro.dci.auth.domain.ports.out.FaceRecognicionPort;

public class AddImageToUserUserCaseImpl implements AddImageToUserUseCase {
    private final FaceRecognicionPort faceRecognicionPort;

    public AddImageToUserUserCaseImpl(FaceRecognicionPort faceRecognicionPort) {
        this.faceRecognicionPort = faceRecognicionPort;
    }

    @Override
    public boolean addImageToUser(File image, Long userId) {
        return faceRecognicionPort.addImageToUser(image, userId);
    }


}
