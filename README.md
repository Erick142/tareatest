# Arquitectura Puerto y adaptadores - Autenticación biométrica para pequeñas empresas
Este proyecto es parte de la asignatura de Arquitectura de Software, implementado en Java con el framework Spring, con el objetivo de desarrollar un sistema de autenticación biométrica para pequeñas empresas utilizando una arquitectura hexagonal.

El programa hace uso de una API en Docker llamada CompreFace de Exadel-Inc, que permite verificar la semejanza del rostro de las personas.

Con un enfoque de pasarela, este sistema está diseñado para ser utilizado por múltiples empresas que deseen utilizar el servicio. Cuando un usuario es autenticado, obtendrá la API Key de la empresa en la que se está autenticando, lo que le permitirá acceder a los servicios de dicha empresa.

La autenticacion de usuario se considera existoso cuando en la respuesta del endpoint api/company/{id}/authenticate viene la Secret key de la empresa en donde se autentica el usuario. sino, se considera no autenticado.
## Requisitos
- Tener docker instalado
- Se recomienda ejecutarlo en una distribucion de linux porque la API externa utiliza muchos recursos.
- (si se presentan errores) Crear una cuenta en la api local de CompreFace en localhost:8000 y crear api keys.
![imagen de ejemplo](./doc/image.png)

## Configuración
Para ejecutar el proyecto, utiliza el siguiente comando 
```bash
docker compose up --build

```
### Consideraciones
Se debe ejecutar una peticion DELETE a http://{tu_ip}:8000/api/v1/recognition/subjects cada vez que se reinician los contenedores, debido a que la api externa usa una base de datos estatica y la aplicacion usa una base de datos en tiempo de ejecucion. si no se realiza esa peticion se generar errores con los id's de los registros.

# Documentación
## Endpoints de la API puerto 8080
A continuación se enumeran las rutas de la API con sus métodos HTTP correspondientes y ejemplos de entrada:
1. Crear una empresa
   Método: POST
   Ruta: /api/company
   Input: JSON con la información de la empresa (name, secretKey y users)

```json
{
    "name": "company updated",
    "secretKey": "Mi secret key super secreta de la compañia",
    "users": []
}
```
2. Obtener todas las empresas
   Método: GET
   Ruta: /api/company

3. Obtener empresa por id
   Método: GET
   Ruta: /api/company/{id}
   Input: url con el id de la empresa en params

4. Eliminar empresa por id
   Método: DELETE
   Ruta: /api/company/{id}
   Input: url con el id de la empresa en params

5. Editar empresa por id
   Método: PUT
   Ruta: /api/company/{id}
   Input: JSON con la información de la empresa (name, secretKey y users)

```json
{
    "name": "company updated",
    "secretKey": "Mi secret key super secreta de la compañia",
    "users": []
}
```

6. Crear usuario
   Método: POST
   Ruta: /api/user
   Input: Form Data con la información del usuario (name y image)

```form-data

   name: "user 1",
   image: (imagen del usuario, puede ser en png, jpg o webp)

```
7. Obtener todas los usuarios
   Método: GET
   Ruta: /api/user

8. Obtener usuario por id
   Método: GET
   Ruta: /api/user/{id}
   Input: url con el id del usuario en params

9. Eliminar Usuario por id
   Método: DELETE
   Ruta: /api/user/{id}
   Input: url con el id del usuario en params

10. Editar usuario por id
   Método: PUT
   Ruta: /api/company/{id}
   Input: JSON con la información del usuario (name)

```json
{
    "name": "el usuario"
}
```

11. Agregar usuario a empresa
   Método: PATCH
   Ruta: /api/company/{companyId}/users/{userId}/add
   Input: url con el id de la empresa y el usuario por params.
12. Quitar usuario de empresa
   Método: PATCH
   Ruta: /api/company/{companyId}/users/{userId}/remove
   Input: url con el id de la empresa y el usuario por params.
13. Autenticar
   Método: POST
   Ruta: /api/company/{companyId}/authenticate
   Input: url con el id de la empresa y archivo de la imagen del usuario que se quiere autenticar
```form-data

   image: (imagen del usuario, puede ser en png, jpg o webp)

```
## Endpoint de la API externa
También se crearon endpoints para que se puedan cambiar las Api keys de la api externa, esto no incluido en los casos de uso ya que son endpoints si hay errores.

1. Obtener api keys actuales
   Método: GET
   Ruta: /api/face
2. Cambiar api key de la funcion de reconocimiento
   Método: PATCH
   Ruta: /api/face/recognition
   Input: Form data con atributo llamado key.
```form-data

   key: "1203123-123120301-123123123"

```

## Diagrama de Clase
El diagrama clases ofrece una representación gráfica de la organización del programa y las conexiones entre las clases que lo conforman. El diagrama presentado a continuación ilustra las clases presentes en la aplicación y sus respectivas interacciones.
![diagrama de clases](./doc/Class%20Diagram1.jpg)
## Diagrama de Caso de Uso
### Lista de actores:


- **ACT-01:** Usuario.

- **ACT-02:** Compañia.

- **ACT-03:** CompreFace API.

### Lista de casos de uso:


- **CU-01:** Crear usuario

- **CU-02:** Autenticar
- **CU-03:** Editar Usuario
- **CU-04:** Desplegar usuarios
- **CU-05:** Desplegar usuario por id
- **CU-06:** Desplegar todos los usuarios
- **CU-07:** Eliminar usuario
- **CU-08:** Crear empresa
- **CU-09:** Editar empresa
- **CU-10:** Eliminar empresa
- **CU-12:** Desplegar empresa

- **CU-13:** Desplegar empresa por id

- **CU-14:** Desplegar todas las empresas

- **CU-15:** Desplegar empresas por usuario

- **CU-16:** Añadir usuario a empresa

- **CU-17:** Quitar usuario de empresas


![diagrama de caso de uso](./doc/Use%20Case%20Diagram1.jpg)
## Estructura del Proyecto

En el nivel del Dominio se encuentran las entidades fundamentales del negocio y sus interrelaciones, junto con la lógica empresarial correspondiente. Estas entidades son independientes de la infraestructura y de cómo se implementan, lo que facilita el enfoque en las reglas y restricciones del negocio.

La capa de Aplicación alberga los casos de uso que representan las diversas acciones o funcionalidades que la aplicación puede llevar a cabo. Estos casos de uso gestionan la comunicación entre los puertos de entrada (interfaces que describen las acciones disponibles desde el exterior) y los puertos de salida (interfaces que indican las acciones que la aplicación puede realizar hacia el exterior, como interactuar con bases de datos o servicios externos).

En cuanto a la capa de Infraestructura, aquí se encuentran los adaptadores y la implementación de los puertos de salida, así como la configuración y la interacción con servicios externos. Los adaptadores tienen la tarea de transformar las solicitudes externas en llamadas a los casos de uso y de convertir las respuestas de los casos de uso en formatos comprensibles para los sistemas externos.
```
└── 📁auth
    └── 📁application
        └── 📁services
        └── 📁usecases
    └── 📁domain
        └── 📁model
        └── 📁ports
            └── 📁in
            └── 📁out
    └── 📁infrastructure
        └── 📁adapters
        └── 📁config
        └── 📁controllers
        └── 📁entities
        └── 📁repositories
```
