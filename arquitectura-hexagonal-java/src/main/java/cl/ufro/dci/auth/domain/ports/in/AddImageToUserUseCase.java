package cl.ufro.dci.auth.domain.ports.in;

import java.io.File;


public interface AddImageToUserUseCase {
    boolean addImageToUser(File image, Long userId);
}
