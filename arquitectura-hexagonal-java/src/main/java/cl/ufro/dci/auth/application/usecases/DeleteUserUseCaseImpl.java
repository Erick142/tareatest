package cl.ufro.dci.auth.application.usecases;

import cl.ufro.dci.auth.domain.ports.in.DeleteUserUseCase;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;

public class DeleteUserUseCaseImpl implements DeleteUserUseCase{
    public DeleteUserUseCaseImpl(UserRepositoryPort userRepositoryPort) {
        this.userRepositoryPort = userRepositoryPort;
    }

    private final UserRepositoryPort userRepositoryPort;

    @Override
    public boolean deleteUser(Long id) {
        return userRepositoryPort.deleteById(id);
    }
    
}
