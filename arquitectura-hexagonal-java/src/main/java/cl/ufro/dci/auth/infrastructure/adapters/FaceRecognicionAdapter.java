package cl.ufro.dci.auth.infrastructure.adapters;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import cl.ufro.dci.auth.application.services.CompanyService;
import cl.ufro.dci.auth.application.services.UserService;
import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.model.UserRecognition;
import cl.ufro.dci.auth.domain.ports.out.FaceRecognicionPort;

@Component
public class FaceRecognicionAdapter implements FaceRecognicionPort{
    //constantes
    private final RestTemplate restTemplate;
    private final String domain = "compreface-fe";
    //api keys
    private String faceRecognitionKey = "00000000-0000-0000-0000-000000000002";
    private String faceDetectionKey = "00000000-0000-0000-0000-000000000003";
    private String faceVerificationKey = "00000000-0000-0000-0000-000000000004";
    //services
    private final UserService userService;
    private final CompanyService companyService;

    //contructor
    public FaceRecognicionAdapter(UserService userService, CompanyService companyService) {
        restTemplate = new RestTemplate();
        this.userService = userService;
        this.companyService = companyService;
    }
    //getter setter
    public String getFaceRecognitionKey() {
        return faceRecognitionKey;
    }

    public void setFaceRecognitionKey(String faceRecognitionKey) {
        this.faceRecognitionKey = faceRecognitionKey;
    }

    public String getFaceDetectionKey() {
        return faceDetectionKey;
    }

    public void setFaceDetectionKey(String faceDetectionKey) {
        this.faceDetectionKey = faceDetectionKey;
    }

    public String getFaceVerificationKey() {
        return faceVerificationKey;
    }

    public void setFaceVerificationKey(String faceVerificationKey) {
        this.faceVerificationKey = faceVerificationKey;
    }

    //funcitions
    
    /**
     * Autentica a un usuario mediante reconocimiento facial.
     * 
     * @param image     La imagen del usuario para el reconocimiento facial.
     * @param companyId El ID de la empresa a la que pertenece el usuario.
     * @return          Un objeto Optional que contiene el resultado del reconocimiento facial del usuario, si es exitoso.
     *                  Si la autenticación falla o hay algún error, retorna Optional.empty().
     */
    @Override
    public Optional<UserRecognition> authenticate(File image, Long companyId) {
        try {
            String url = "http://" + domain + "/api/v1/recognition/recognize";

            // Configuración de encabezados HTTP
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.set("x-api-key", faceRecognitionKey);

            // Construcción del cuerpo de la solicitud
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            FileSystemResource fileSystemResource = new FileSystemResource(image);
            body.add("file", fileSystemResource);

            // Construcción de la entidad HTTP
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            // Realización de la solicitud al servicio de reconocimiento facial
            ResponseEntity<RecognitionResult> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, RecognitionResult.class);
            
            // Obtención del resultado del reconocimiento facial
            RecognitionResult recognitionResult = responseEntity.getBody();

            // Obtención de la similitud y el ID del usuario reconocido
            double similitud = recognitionResult.result.get(0).subjects.get(0).similarity;
            Long userId = Long.parseLong(recognitionResult.result.get(0).subjects.get(0).subject);

            // Obtención del usuario a partir de su ID
            User user = userService.getUserById(userId).get();

            // Creación del objeto UserRecognition con los datos obtenidos
            UserRecognition userRecognition = new UserRecognition(user, similitud, null);

            // Obtención de la empresa a partir de su ID
            Company company = companyService.getCompanyById(companyId).get();

            // Validaciones
            if (!company.haveUser(userId)) {
                return Optional.empty(); // El usuario no pertenece a esta empresa
            }
            if (userRecognition.isValid()) {
                userRecognition.setKey(company.getSecretKey()); // Asignar clave secreta si la autenticación es válida
            }
            return Optional.of(userRecognition); // Retorna el resultado del reconocimiento facial del usuario
        } catch (Exception e) {
            return Optional.empty(); // Retorna vacío si ocurre algún error durante la autenticación
        }
    }


    /**
     * Añade una imagen del usuario a una API externa para su reconocimiento facial en el futuro.
     * 
     * @param image  La imagen del usuario a añadir para el reconocimiento facial.
     * @param userId El ID del usuario al que pertenece la imagen.
     * @return       true si la imagen del usuario se añade correctamente a la API externa,
     *               false si hay algún error o la operación no es exitosa.
     */
    @Override
    public boolean addImageToUser(File image, Long userId) {
        try {
            String url = "http://" + domain + "/api/v1/recognition/faces?subject=" + userId.toString();

            // Configuración de encabezados HTTP
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.set("x-api-key", faceRecognitionKey);
            
            // Construcción del cuerpo de la solicitud
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            FileSystemResource fileSystemResource = new FileSystemResource(image);
            body.add("file", fileSystemResource);

            // Construcción de la entidad HTTP
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            // Realización de la solicitud para añadir la imagen del usuario a la API externa
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
            
            // Verificación del estado de la respuesta para determinar el éxito de la operación
            if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
                return true; // La imagen del usuario se añadió correctamente
            } else {
                return false; // La operación no fue exitosa
            }
        } catch (Exception e) {
            return false; // Retorna false si ocurre algún error durante el proceso
        }
    }


    /**
     * Añade un nuevo usuario a la API externa para el reconocimiento facial.
     * 
     * @param userId El ID del usuario a añadir a la API externa.
     * @return       true si el usuario se añade correctamente a la API externa,
     *               false si hay algún error o la operación no es exitosa.
     */
    @Override
    public boolean addSubject(Long userId) {
        try {
            String url = "http://" + domain + "/api/v1/recognition/subjects";
            
            // Configuración de encabezados HTTP
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("x-api-key", faceRecognitionKey);
            
            // Construcción del objeto NewSubject para representar al usuario
            NewSubject subject = new NewSubject();
            subject.subject = userId.toString();

            // Construcción de la entidad HTTP
            HttpEntity<NewSubject> requestEntity = new HttpEntity<>(subject, headers);
            
            // Realización de la solicitud para añadir el usuario a la API externa
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
            
            // Verificación del estado de la respuesta para determinar el éxito de la operación
            if (responseEntity.getStatusCode() != HttpStatus.CREATED) {
                return false; // La operación no fue exitosa
            }

            return true; // El usuario se añadió correctamente a la API externa
        } catch (Exception e) {
            return false; // Retorna false si ocurre algún error durante el proceso
        }
    }

    /**
     * Obtiene las claves de la API externa de reconocimiento facial.
     * 
     * @return Mapa con las claves.
     */
    public Map<String, String> getApiKeys(){
        HashMap<String, String> keys = new HashMap<>();
        keys.put("faceRecognitionKey", faceRecognitionKey);
        keys.put("faceDetectionKey", faceDetectionKey);
        keys.put("faceVerificationKey", faceVerificationKey);

        return keys;
    }


    //place holder data objects
    private static class NewSubject{
        public String subject;
    }

    private static class RecognitionResult {
        public List<ResultEntry> result;

        private static class ResultEntry {
            public Box box;
            public List<Subject> subjects;
    
            private static class Box {
                public double probability;
                public int x_max;
                public int y_max;
                public int x_min;
                public int y_min;
            }
        
            private static class Subject {
                public String subject;
                public double similarity;
            }
        }
    }
}
