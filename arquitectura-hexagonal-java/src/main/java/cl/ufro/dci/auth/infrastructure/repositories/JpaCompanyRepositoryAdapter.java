package cl.ufro.dci.auth.infrastructure.repositories;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;
import cl.ufro.dci.auth.infrastructure.entities.CompanyEntity;

@Component
public class JpaCompanyRepositoryAdapter implements CompanyRepositoryPort{
    private final JpaCompanyRepository jpaCompanyRepository;

    public JpaCompanyRepositoryAdapter(JpaCompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    @Override
    public Company save(Company company) {
        CompanyEntity companyEntity = CompanyEntity.fromDomainModel(company);
        CompanyEntity savedCompanyEntity = jpaCompanyRepository.save(companyEntity);
        return savedCompanyEntity.toDomainModel();
    }

    @Override
    public Optional<Company> findById(Long id) {
        return jpaCompanyRepository.findById(id).map(CompanyEntity::toDomainModel);
    }

    @Override
    public List<Company> findAll() {
        return jpaCompanyRepository.findAll().stream().map(CompanyEntity::toDomainModel).collect(Collectors.toList());
    }

    @Override
    public Optional<Company> update(Company company) {
        if(jpaCompanyRepository.existsById(company.getId())){
            CompanyEntity companyEntity = CompanyEntity.fromDomainModel(company);
            CompanyEntity updatedCompanyEntity = jpaCompanyRepository.save(companyEntity);
            return Optional.of(updatedCompanyEntity.toDomainModel());
        }
        return Optional.empty();
    }

    @Override
    public boolean deleteById(Long id) {
        if(jpaCompanyRepository.existsById(id)){
            jpaCompanyRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Company> findCompaniesByUserId(Long id) {
        return jpaCompanyRepository.findCompaniesByUserId(id).stream().map(CompanyEntity::toDomainModel).toList();
    }
}
