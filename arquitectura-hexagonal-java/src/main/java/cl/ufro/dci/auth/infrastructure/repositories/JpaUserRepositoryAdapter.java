package cl.ufro.dci.auth.infrastructure.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;
import cl.ufro.dci.auth.infrastructure.entities.UserEntity;

@Component
public class JpaUserRepositoryAdapter implements UserRepositoryPort {
    
    private final JpaUserRepository jpaUserRepository;

    public JpaUserRepositoryAdapter(JpaUserRepository jpaUserRepository) {
        this.jpaUserRepository = jpaUserRepository;
    }

    @Override
    public User save(User user) {
        UserEntity savedUser = jpaUserRepository.save(UserEntity.fromDomainModel(user));
        return savedUser.toDomainModel();
    }

    @Override
    public Optional<User> findById(Long id) {
        return jpaUserRepository.findById(id).map(UserEntity::toDomainModel);
    }

    @Override
    public List<User> findAll() {
        return jpaUserRepository.findAll().stream().map(UserEntity::toDomainModel).toList();
    }

    @Override
    public Optional<User> update(User user) {
        if(jpaUserRepository.existsById(user.getId())){
            UserEntity userEntity = UserEntity.fromDomainModel(user);
            UserEntity updatedUserEntity = jpaUserRepository.save(userEntity);
            return Optional.of(updatedUserEntity.toDomainModel());
        }
        return Optional.empty();
    }

    @Override
    public boolean deleteById(Long id) {
        if(jpaUserRepository.existsById(id)){
            jpaUserRepository.deleteById(id);
            return true;
        }
        return false;
    }

}
