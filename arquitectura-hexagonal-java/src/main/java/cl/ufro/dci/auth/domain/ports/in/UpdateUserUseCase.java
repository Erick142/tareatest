package cl.ufro.dci.auth.domain.ports.in;

import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;

public interface UpdateUserUseCase {
    Optional<User> updateUser(Long id, User user);
}
