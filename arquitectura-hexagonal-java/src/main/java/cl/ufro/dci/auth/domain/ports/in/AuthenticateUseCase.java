package cl.ufro.dci.auth.domain.ports.in;

import java.io.File;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.UserRecognition;

public interface AuthenticateUseCase {
    Optional<UserRecognition> authenticate(File image, Long companyId);
}
