package cl.ufro.dci.auth.application.usecases;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.ports.in.CreateCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;

public class CreateCompanyUseCaseImpl implements CreateCompanyUseCase {

    private final CompanyRepositoryPort companyRepositoryPort;

    public CreateCompanyUseCaseImpl(CompanyRepositoryPort companyRepositoryPort) {
        this.companyRepositoryPort = companyRepositoryPort;
    }

    @Override
    public Company createCompany(Company company) {
        return companyRepositoryPort.save(company);
    }
    
}
