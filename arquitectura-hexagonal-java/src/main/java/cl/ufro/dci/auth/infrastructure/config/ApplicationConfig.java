package cl.ufro.dci.auth.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cl.ufro.dci.auth.application.services.CompanyService;
import cl.ufro.dci.auth.application.services.UserService;
import cl.ufro.dci.auth.application.usecases.*;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;
import cl.ufro.dci.auth.infrastructure.repositories.JpaCompanyRepositoryAdapter;
import cl.ufro.dci.auth.infrastructure.repositories.JpaUserRepositoryAdapter;

@Configuration
public class ApplicationConfig {


    //Company
    @Bean 
    public CompanyService companyService(CompanyRepositoryPort companyRepositoryPort, UserRepositoryPort userRepositoryPort){
        return new CompanyService(
            new CreateCompanyUseCaseImpl(companyRepositoryPort),
            new RetrieveCompanyUseCaseImpl(companyRepositoryPort),
            new DeleteCompanyUseCaseImpl(companyRepositoryPort),
            new UpdateCompanyUseCaseImpl(companyRepositoryPort),
            new AddUserToCompanyUseCaseImpl(companyRepositoryPort, userRepositoryPort),
            new RemoveUserFromCompanyUseCaseImpl(companyRepositoryPort, userRepositoryPort)
        );
    }

    @Bean
    public CompanyRepositoryPort companyRepositoryPort(JpaCompanyRepositoryAdapter jpaCompanyRepositoryAdapter) {
        return jpaCompanyRepositoryAdapter;
    }

    //user
    @Bean
    public UserService userService(UserRepositoryPort userRepositoryPort){
        return new UserService(
            new CreateUserUseCaseImpl(userRepositoryPort),
            new DeleteUserUseCaseImpl(userRepositoryPort),
            new RetrieveUserUseCaseImpl(userRepositoryPort),
            new UpdateUserUseCaseImpl(userRepositoryPort)
        );
    }
    @Bean
    public UserRepositoryPort userRepositoryPort(JpaUserRepositoryAdapter jpaUserRepositoryAdapter){
        return jpaUserRepositoryAdapter;
    }
}
