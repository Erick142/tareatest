package cl.ufro.dci.auth.application.usecases;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.ports.in.RetrieveCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;

public class RetrieveCompanyUseCaseImpl implements RetrieveCompanyUseCase {

    private final CompanyRepositoryPort companyRepositoryPort;

    public RetrieveCompanyUseCaseImpl(CompanyRepositoryPort companyRepositoryPort) {
        this.companyRepositoryPort = companyRepositoryPort;
    }

    @Override
    public Optional<Company> getCompanyById(Long id) {
        return companyRepositoryPort.findById(id);
    }

    @Override
    public List<Company> getAllCompanies() {
        return companyRepositoryPort.findAll();
    }

    @Override
    public List<Company> findCompaniesByUserId(Long userId) {
        return companyRepositoryPort.findCompaniesByUserId(userId);
    }

}
