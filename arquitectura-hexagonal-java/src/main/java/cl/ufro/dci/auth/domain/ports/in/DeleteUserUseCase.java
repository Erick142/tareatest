package cl.ufro.dci.auth.domain.ports.in;

public interface DeleteUserUseCase {
    boolean deleteUser(Long id);
}
