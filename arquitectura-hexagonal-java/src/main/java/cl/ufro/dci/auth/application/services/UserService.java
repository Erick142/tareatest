package cl.ufro.dci.auth.application.services;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.in.CreateUserUseCase;
import cl.ufro.dci.auth.domain.ports.in.DeleteUserUseCase;
import cl.ufro.dci.auth.domain.ports.in.RetrieveUserUseCase;
import cl.ufro.dci.auth.domain.ports.in.UpdateUserUseCase;

public class UserService implements CreateUserUseCase, RetrieveUserUseCase, UpdateUserUseCase, DeleteUserUseCase{
    private final CreateUserUseCase createUserUseCase;
    private final RetrieveUserUseCase retrieveUserUseCase;
    private final UpdateUserUseCase updateUserUseCase;
    private final DeleteUserUseCase deleteUserUseCase;

    public UserService(CreateUserUseCase createUserUseCase, DeleteUserUseCase deleteUserUseCase, RetrieveUserUseCase retrieveUserUseCase, UpdateUserUseCase updateUserUseCase) {
        this.createUserUseCase = createUserUseCase;
        this.retrieveUserUseCase = retrieveUserUseCase;
        this.updateUserUseCase = updateUserUseCase;
        this.deleteUserUseCase = deleteUserUseCase;
    }

    /**
     * Elimina un usuario por su ID.
     * 
     * @param id El ID del usuario a eliminar.
     * @return   true si el usuario se elimina correctamente, false si no se pudo eliminar.
     */
    @Override
    public boolean deleteUser(Long id) {
        return deleteUserUseCase.deleteUser(id);
    }

    /**
     * Actualiza un usuario por su ID.
     * 
     * @param id   El ID del usuario a actualizar.
     * @param user La nueva información del usuario.
     * @return     El usuario actualizado, si se actualiza correctamente.
     */
    @Override
    public Optional<User> updateUser(Long id, User user) {
        return updateUserUseCase.updateUser(id, user);
    }

    /**
     * Obtiene un usuario por su ID.
     * 
     * @param id El ID del usuario.
     * @return   El usuario encontrado, si existe.
     */
    @Override
    public Optional<User> getUserById(Long id) {
        return retrieveUserUseCase.getUserById(id);
    }

    /**
     * Obtiene todos los usuarios.
     * 
     * @return Lista de todos los usuarios.
     */
    @Override
    public List<User> getAllUsers() {
        return retrieveUserUseCase.getAllUsers();
    }

    /**
     * Crea un nuevo usuario.
     * 
     * @param user El usuario a crear.
     * @return     El usuario creado.
     */
    @Override
    public User createUser(User user) {
        return createUserUseCase.createUser(user);
    }
}
