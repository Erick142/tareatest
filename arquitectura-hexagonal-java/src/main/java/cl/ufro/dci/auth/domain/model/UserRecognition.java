package cl.ufro.dci.auth.domain.model;

public class UserRecognition {
    private User user;
    private double similarity;
    private String key;

    
    public UserRecognition() {
    }
    
    public UserRecognition(User user, double similarity, String key) {
        this.user = user;
        this.similarity = similarity;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    
    public double getSimilarity() {
        return similarity;
    }
    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }

    public boolean isValid(){
        return similarity > 0.9;
    }
}
