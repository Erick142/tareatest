package cl.ufro.dci.auth.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.ufro.dci.auth.infrastructure.entities.CompanyEntity;
@Repository
public interface JpaCompanyRepository extends JpaRepository<CompanyEntity, Long>{
    @Query("SELECT c FROM CompanyEntity c JOIN c.users u WHERE u.id = :userId")
    List<CompanyEntity> findCompaniesByUserId(@Param("userId") Long userId);
}
