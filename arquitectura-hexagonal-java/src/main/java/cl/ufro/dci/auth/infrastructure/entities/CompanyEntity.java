package cl.ufro.dci.auth.infrastructure.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.model.User;

@Entity
public class CompanyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String secretKey;
    @ManyToMany
    private List<UserEntity> users;
    
    public CompanyEntity() {
        users = new ArrayList<>();
    }

    public CompanyEntity(Long id, String name, String secretKey, List<UserEntity> users) {
        this.id = id;
        this.name = name;
        this.secretKey = secretKey;
        this.users = users;
    }

    public static CompanyEntity fromDomainModel(Company company){
        ArrayList<UserEntity> userEntities = new ArrayList<>();
        for (User user : company.getUsers()) {
            userEntities.add(UserEntity.fromDomainModel(user));
        }
        return new CompanyEntity(company.getId(), company.getName(), company.getSecretKey(), userEntities);
    }

    public Company toDomainModel(){
        List<User> users = new ArrayList<>();
        for (UserEntity userEntity : this.users) {
            users.add(userEntity.toDomainModel());
        }

        return new Company(id, name, secretKey, users);
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSecretKey() {
        return secretKey;
    }
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    public List<UserEntity> getUsers() {
        return users;
    }
    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
