package cl.ufro.dci.auth.infrastructure.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.ufro.dci.auth.application.services.UserService;
import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.out.FaceRecognicionPort;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;
    private final FaceRecognicionPort faceRecognicionPort;

    public UserController(UserService userService, FaceRecognicionPort faceRecognicionPort) {
        this.userService = userService;
        this.faceRecognicionPort = faceRecognicionPort;
    }
    
    /**
     * Crea un nuevo usuario con una imagen y un nombre.
     * 
     * @param image La imagen del usuario.
     * @param name  El nombre del usuario.
     * @return      ResponseEntity con el usuario creado y el código de estado HTTP correspondiente.
     * @throws IOException Si ocurre un error durante la transferencia o manipulación de la imagen.
     */
    @PostMapping
    public ResponseEntity<Object> createUser(@RequestParam MultipartFile image,
    @RequestParam String name) throws IOException{
        File tempFile = Files.createTempFile("temp", image.getOriginalFilename()).toFile();
        image.transferTo(tempFile);

        User user = new User(name);
        User createdUser = userService.createUser(user);
        boolean inAPI = faceRecognicionPort.addSubject(createdUser.getId());
        if(!inAPI){
            userService.deleteUser(createdUser.getId());
            return new ResponseEntity<>("Error al ingresar el sujeto a la API externa", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        boolean withImages = faceRecognicionPort.addImageToUser(tempFile, createdUser.getId());
        if(!withImages){
            userService.deleteUser(createdUser.getId());
            return new ResponseEntity<>("Error en la API al añadir la imagen", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }

    /**
     * Obtiene un usuario por su ID.
     * 
     * @param userId El ID del usuario.
     * @return       ResponseEntity con el usuario encontrado y el código de estado HTTP correspondiente.
     */
    @GetMapping("/{userId}")
    public ResponseEntity<User> getUser(@PathVariable Long userId){
        Optional<User> optionalUser = userService.getUserById(userId);
        if(optionalUser.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
    }

    /**
     * Obtiene todos los usuarios.
     * 
     * @return ResponseEntity con la lista de usuarios y el código de estado HTTP correspondiente.
     */
    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    /**
     * Actualiza un usuario por su ID.
     * 
     * @param userId El ID del usuario a actualizar.
     * @param user   El nuevo estado del usuario.
     * @return       ResponseEntity con el usuario actualizado y el código de estado HTTP correspondiente.
     */
    @PutMapping("/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable Long userId, @RequestBody User user){
        Optional<User> optionalUser = userService.getUserById(userId);
        if(optionalUser.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User previusUser = optionalUser.get();
        user.setId(previusUser.getId());

        Optional<User> newUser = userService.updateUser(userId, user);
        return new ResponseEntity<>(newUser.get(), HttpStatus.OK);
    }

    /**
     * Elimina un usuario por su ID.
     * 
     * @param userId El ID del usuario a eliminar.
     * @return       ResponseEntity con el código de estado HTTP correspondiente.
     */
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId){
        if(userService.deleteUser(userId)){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}

