package cl.ufro.dci.auth.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.ufro.dci.auth.infrastructure.entities.UserEntity;

@Repository
public interface JpaUserRepository extends JpaRepository<UserEntity, Long>{
    
}
