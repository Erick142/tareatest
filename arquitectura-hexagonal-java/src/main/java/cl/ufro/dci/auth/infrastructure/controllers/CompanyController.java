package cl.ufro.dci.auth.infrastructure.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.ufro.dci.auth.application.services.CompanyService;
import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.model.UserRecognition;
import cl.ufro.dci.auth.domain.ports.out.FaceRecognicionPort;


@RestController
@RequestMapping("/api/company")
public class CompanyController {
    private final CompanyService companyService;
    private final FaceRecognicionPort faceRecognicionPort;

    public CompanyController(CompanyService companyService, FaceRecognicionPort faceRecognicionPort) {
        this.companyService = companyService;
        this.faceRecognicionPort = faceRecognicionPort;
    }
    
    /**
     * Crea una nueva empresa.
     * 
     * @param company La empresa a crear.
     * @return        ResponseEntity con la empresa creada y el código de estado HTTP correspondiente.
     */
    @PostMapping
    public ResponseEntity<Company> createCompany(@RequestBody Company company){
        Company createdCompany = companyService.createCompany(company);
        return new ResponseEntity<>(createdCompany, HttpStatus.CREATED);
    }

    /**
     * Obtiene una empresa por su ID.
     * 
     * @param companyId El ID de la empresa.
     * @return          ResponseEntity con la empresa encontrada y el código de estado HTTP correspondiente.
     */
    @GetMapping("/{companyId}")
    public ResponseEntity<Company> getCompany(@PathVariable Long companyId){
        Optional<Company> optionalCompany = companyService.getCompanyById(companyId);
        if(optionalCompany.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(optionalCompany.get(), HttpStatus.OK);
    }

    /**
     * Obtiene todas las empresas asociadas a un usuario.
     * 
     * @param userId El ID del usuario.
     * @return       ResponseEntity con la lista de empresas y el código de estado HTTP correspondiente.
     */
    @GetMapping("/user/{userId}")
    public ResponseEntity<List<Company>> getAllcompaniesByUserId(@PathVariable Long userId){
        List<Company> companies = companyService.findCompaniesByUserId(userId);
        return new ResponseEntity<>(companies, HttpStatus.OK);
    }

    /**
     * Obtiene todas las empresas.
     * 
     * @return ResponseEntity con la lista de empresas y el código de estado HTTP correspondiente.
     */
    @GetMapping
    public ResponseEntity<List<Company>> getAllCompanies(){
        List<Company> companies = companyService.getAllCompanies();
        return new ResponseEntity<>(companies, HttpStatus.OK);
    }

    /**
     * Actualiza una empresa por su ID.
     * 
     * @param companyId El ID de la empresa a actualizar.
     * @param company   La nueva información de la empresa.
     * @return          ResponseEntity con la empresa actualizada y el código de estado HTTP correspondiente.
     */
    @PutMapping("/{companyId}")
    public ResponseEntity<Company> updateCompany(@PathVariable Long companyId, @RequestBody Company company){
        Optional<Company> optionaCompany = companyService.getCompanyById(companyId);
        if(optionaCompany.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Company previusCompany = optionaCompany.get();
        company.setId(previusCompany.getId());

        Optional<Company> updatedCompany = companyService.updateCompany(companyId, company);
        return new ResponseEntity<>(updatedCompany.get(), HttpStatus.OK);
    }

    /**
     * Elimina una empresa por su ID.
     * 
     * @param companyId El ID de la empresa a eliminar.
     * @return          ResponseEntity con el código de estado HTTP correspondiente.
     */
    @DeleteMapping("/{companyId}")
    public ResponseEntity<Void> deleteCompany(@PathVariable Long companyId){
        if(companyService.deleteCompany(companyId)){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Añade un usuario a una empresa.
     * 
     * @param companyId El ID de la empresa.
     * @param userId    El ID del usuario a añadir.
     * @return          ResponseEntity con la empresa actualizada y el código de estado HTTP correspondiente.
     */
    @PatchMapping("/{companyId}/users/{userId}/add")
    public ResponseEntity<Company> addUserToCompany(@PathVariable Long companyId, @PathVariable Long userId){
        Optional<Company> optionalUpdatedCompany = companyService.addUserToCompany(companyId, userId);
        if(optionalUpdatedCompany.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(optionalUpdatedCompany.get(), HttpStatus.OK);
    }

    /**
     * Elimina un usuario de una empresa.
     * 
     * @param companyId El ID de la empresa.
     * @param userId    El ID del usuario a eliminar.
     * @return          ResponseEntity con la empresa actualizada y el código de estado HTTP correspondiente.
     */
    @PatchMapping("/{companyId}/users/{userId}/remove")
    public ResponseEntity<Company> removeUserFromCompany(@PathVariable Long companyId, @PathVariable Long userId){
        Optional<Company> optionalUpdatedCompany = companyService.removeUserFromCompany(companyId, userId);
        if(optionalUpdatedCompany.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(optionalUpdatedCompany.get(), HttpStatus.OK);
    }

    /**
     * Realiza la autenticación facial de un usuario para una empresa.
     * 
     * @param image      La imagen del usuario para la autenticación facial.
     * @param companyId  El ID de la empresa.
     * @return           ResponseEntity con el resultado de la autenticación facial y el código de estado HTTP correspondiente.
     * @throws IOException Si ocurre un error durante la transferencia o manipulación de la imagen.
     */
    @PostMapping("/{companyId}/authenticate")
    public ResponseEntity<Object> auth(@RequestParam MultipartFile image,
    @PathVariable Long companyId) throws IOException{

        File tempFile = Files.createTempFile("temp", image.getOriginalFilename()).toFile();
        image.transferTo(tempFile);

        //Si la empresa no existe
        if(companyService.getCompanyById(companyId).isEmpty()){
            Map<String, String> body = Map.of("message","La empresa no existe");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
        }

        Optional<UserRecognition> userRecognition = faceRecognicionPort.authenticate(tempFile, companyId);
        
        //Si el usuario no forma parte de la empresa
        if(userRecognition.isEmpty()){
            Map<String, String> body = Map.of("message", "No perteneces a la empresa");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
        }

        return new ResponseEntity<>(userRecognition.get(), HttpStatus.OK);
    }
}
