package cl.ufro.dci.auth.domain.ports.in;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;

public interface RetrieveUserUseCase {
    Optional<User> getUserById(Long id);
    List<User> getAllUsers();
}
