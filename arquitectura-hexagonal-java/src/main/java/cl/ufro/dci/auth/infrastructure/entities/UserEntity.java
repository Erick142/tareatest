package cl.ufro.dci.auth.infrastructure.entities;


import cl.ufro.dci.auth.domain.model.User;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public UserEntity(){}

    public UserEntity(Long id, String name ) {
        this.id = id;
        this.name = name;
    }

    public static UserEntity fromDomainModel(User user){
        return new UserEntity(user.getId(), user.getName());
    }

    public User toDomainModel(){
        return new User(id, name);
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
