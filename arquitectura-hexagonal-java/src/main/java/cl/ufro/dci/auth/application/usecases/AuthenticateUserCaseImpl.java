package cl.ufro.dci.auth.application.usecases;

import java.io.File;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.UserRecognition;
import cl.ufro.dci.auth.domain.ports.in.AuthenticateUseCase;
import cl.ufro.dci.auth.domain.ports.out.FaceRecognicionPort;

public class AuthenticateUserCaseImpl implements AuthenticateUseCase {
    private final FaceRecognicionPort faceRecognicionPort;

    public AuthenticateUserCaseImpl(FaceRecognicionPort faceRecognicionPort) {
        this.faceRecognicionPort = faceRecognicionPort;
    }

    @Override
    public Optional<UserRecognition> authenticate(File image, Long companyId) {
        return faceRecognicionPort.authenticate(image, companyId);
    }
    
}
