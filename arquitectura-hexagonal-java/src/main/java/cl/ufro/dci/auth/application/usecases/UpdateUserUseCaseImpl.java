package cl.ufro.dci.auth.application.usecases;

import java.util.Optional;

import cl.ufro.dci.auth.domain.model.User;
import cl.ufro.dci.auth.domain.ports.in.UpdateUserUseCase;
import cl.ufro.dci.auth.domain.ports.out.UserRepositoryPort;

public class UpdateUserUseCaseImpl implements UpdateUserUseCase {
    private final UserRepositoryPort userRepositoryPort;

    public UpdateUserUseCaseImpl(UserRepositoryPort userRepositoryPort) {
        this.userRepositoryPort = userRepositoryPort;
    }

    @Override
    public Optional<User> updateUser(Long id, User user) {
        return userRepositoryPort.update(user);
    }

    
}
