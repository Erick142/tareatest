package cl.ufro.dci.auth.domain.ports.out;

import cl.ufro.dci.auth.domain.ports.in.AddImageToUserUseCase;
import cl.ufro.dci.auth.domain.ports.in.AuthenticateUseCase;

public interface FaceRecognicionPort extends AuthenticateUseCase, AddImageToUserUseCase{
    boolean addSubject(Long userId);

}
