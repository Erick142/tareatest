package cl.ufro.dci.auth.domain.ports.in;

import cl.ufro.dci.auth.domain.model.Company;

public interface CreateCompanyUseCase {
    Company createCompany(Company company);
}
