package cl.ufro.dci.auth.application.services;

import java.util.List;
import java.util.Optional;

import cl.ufro.dci.auth.domain.model.Company;
import cl.ufro.dci.auth.domain.ports.in.AddUserToCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.in.CreateCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.in.DeleteCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.in.RemoveUserFromCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.in.RetrieveCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.in.UpdateCompanyUseCase;

public class CompanyService implements CreateCompanyUseCase, RetrieveCompanyUseCase, DeleteCompanyUseCase, UpdateCompanyUseCase, AddUserToCompanyUseCase, RemoveUserFromCompanyUseCase{

    private final CreateCompanyUseCase createCompanyUseCase;
    private final RetrieveCompanyUseCase retrieveCompanyUseCase;
    private final DeleteCompanyUseCase deleteCompanyUseCase;
    private final UpdateCompanyUseCase updateCompanyUseCase;
    private final AddUserToCompanyUseCase addUserToCompanyUseCase;
    private final RemoveUserFromCompanyUseCase removeUserFromCompanyUseCase;

    public CompanyService(CreateCompanyUseCase createCompanyUseCase, RetrieveCompanyUseCase retrieveCompanyUseCase, DeleteCompanyUseCase deleteCompanyUseCase, UpdateCompanyUseCase updateCompanyUseCase, AddUserToCompanyUseCase addUserToCompanyUseCase, RemoveUserFromCompanyUseCase removeUserFromCompanyUseCase) {
        this.createCompanyUseCase = createCompanyUseCase;
        this.retrieveCompanyUseCase = retrieveCompanyUseCase;
        this.deleteCompanyUseCase = deleteCompanyUseCase;
        this.updateCompanyUseCase = updateCompanyUseCase;
        this.addUserToCompanyUseCase = addUserToCompanyUseCase;
        this.removeUserFromCompanyUseCase = removeUserFromCompanyUseCase;
    }

    /**
     * Crea una nueva empresa.
     * 
     * @param company La empresa a crear.
     * @return        La empresa creada.
     */
    @Override
    public Company createCompany(Company company) {
        return createCompanyUseCase.createCompany(company);
    }

    /**
     * Obtiene una empresa por su ID.
     * 
     * @param id El ID de la empresa.
     * @return   La empresa encontrada, si existe.
     */
    @Override
    public Optional<Company> getCompanyById(Long id) {
        return retrieveCompanyUseCase.getCompanyById(id);
    }

    /**
     * Obtiene todas las empresas.
     * 
     * @return Lista de todas las empresas.
     */
    @Override
    public List<Company> getAllCompanies() {
        return retrieveCompanyUseCase.getAllCompanies();
    }

    /**
     * Elimina una empresa por su ID.
     * 
     * @param id El ID de la empresa a eliminar.
     * @return   true si la empresa se elimina correctamente, false si no se pudo eliminar.
     */
    @Override
    public boolean deleteCompany(Long id) {
        return deleteCompanyUseCase.deleteCompany(id);
    }

    /**
     * Actualiza una empresa por su ID.
     * 
     * @param id      El ID de la empresa a actualizar.
     * @param company La nueva información de la empresa.
     * @return        La empresa actualizada, si se actualiza correctamente.
     */
    @Override
    public Optional<Company> updateCompany(Long id, Company company) {
        return updateCompanyUseCase.updateCompany(id, company);
    }

    /**
     * Añade un usuario a una empresa.
     * 
     * @param companyId El ID de la empresa.
     * @param userId    El ID del usuario a añadir.
     * @return          La empresa con el usuario añadido, si la operación es exitosa.
     */
    @Override
    public Optional<Company> addUserToCompany(Long companyId, Long userId) {
        return addUserToCompanyUseCase.addUserToCompany(companyId, userId);
    }

    /**
     * Encuentra todas las empresas asociadas a un usuario.
     * 
     * @param userId El ID del usuario.
     * @return       Lista de empresas asociadas al usuario.
     */
    @Override
    public List<Company> findCompaniesByUserId(Long userId) {
        return retrieveCompanyUseCase.findCompaniesByUserId(userId);
    }

    /**
     * Elimina un usuario de una empresa.
     * 
     * @param companyId El ID de la empresa.
     * @param userId    El ID del usuario a eliminar.
     * @return          La empresa con el usuario eliminado, si la operación es exitosa.
     */
    @Override
    public Optional<Company> removeUserFromCompany(Long companyId, Long userId) {
        return removeUserFromCompanyUseCase.removeUserFromCompany(companyId, userId);
    }    

}
