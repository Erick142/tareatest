package cl.ufro.dci.auth.domain.ports.in;

import cl.ufro.dci.auth.domain.model.User;

public interface CreateUserUseCase {
    User createUser(User user);
}
