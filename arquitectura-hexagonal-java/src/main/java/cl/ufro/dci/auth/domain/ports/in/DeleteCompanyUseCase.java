package cl.ufro.dci.auth.domain.ports.in;

public interface DeleteCompanyUseCase {
    boolean deleteCompany(Long id);
}
