package cl.ufro.dci.auth.application.usecases;

import cl.ufro.dci.auth.domain.ports.in.DeleteCompanyUseCase;
import cl.ufro.dci.auth.domain.ports.out.CompanyRepositoryPort;

public class DeleteCompanyUseCaseImpl implements DeleteCompanyUseCase{
    private final CompanyRepositoryPort companyRepositoryPort;

    public DeleteCompanyUseCaseImpl(CompanyRepositoryPort companyRepositoryPort) {
        this.companyRepositoryPort = companyRepositoryPort;
    }

    @Override
    public boolean deleteCompany(Long id) {
        return companyRepositoryPort.deleteById(id);
    }
}
